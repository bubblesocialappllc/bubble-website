'use strict';

// Declare app level module which depends on views, and components
angular.module('bubbleWebsite', [
    'ngRoute',
    'ngMaterial',
    'bubbleWebsite.view1',
    'bubbleWebsite.view2',
    'bubbleWebsite.version'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/view1'});
}]);

angular
    .module('bubbleWebsite', ['ngMaterial', 'users'])
    .config(function( $mdIconProvider ){

        // Register the user `avatar` icons
        $mdIconProvider
            .defaultIconSet("./assets/svg/avatars.svg", 128)
            .icon("menu", "./assets/svg/menu.svg", 24)
            .icon("share", "./assets/svg/share.svg", 24);


    })
    .run(function($log){
        $log.debug("starterApp + ngMaterial + 'users' + 'icon sets' running...");
    });
